#include <msp430g2553.h>
#include <stdint.h>
#include <Includes/i2c_func.h>


/**
 *  main.c
 */

//int8_t Hum_Temp_Status_Reg;
int i;
uint8_t Read_Buffer_Array[6];
uint16_t C1,C2,C3,C4,C5,C6;

/*
 * C1 Pressure Sensitivity
 * C2 Pressure offset
 * C3 Temperature coefficient of pressure sensitivity
 * C4 Temperature coefficient of pressure offset
 * C5 Reference Temperature
 * C6 Temperature coefficient of the temperature
 */

uint8_t ms_crc;
float dpv, dtv;

/*
 * dpv Digital pressure value
 * dtv Digital temperature value
 */
//
//
float dT = 0;
float Temp = 0;

/*
 * dT Difference coefficient between actual and reference temperature
 * Temp Actual temperature
 */

float OFF = 0.0;
float SENS = 0.0;
float P = 0.0;

/*
 * OFF Offset at actual temperature
 * SENS Sensitivity at actual temperature
 * Temperature compensated pressure
 *
 */

uint8_t dTd = 0;
uint8_t dTu = 0;
uint8_t dTdd = 0;
uint8_t dTddd = 0;
uint16_t tempTemp = 0;


uint8_t dPm = 0;
uint8_t dPc = 0;
uint8_t dPd = 0;
uint8_t dPu = 0;
uint8_t dPdd = 0;
uint8_t dPddd = 0;
uint32_t tempP = 0;



/**
 * @brief [Configuracion del USCIB0]
 * @details [Configuracion de USCIB0 para operar como i2c modo maestro]
 */

void ConfigUSCIB0_I2C(void)
{
    //Before doing anything else, put the UCS_B0 port into reset
    P1SEL |= BIT6 + BIT7;                     // Assign I2C pins to USCI_B0
    P1SEL2|= BIT6 + BIT7;                     // Assign I2C pins to USCI_B0
    UCB0CTL1 |= UCSWRST;

    UCB0CTL0 = (UCMST | UCMODE_3  | UCSYNC);
    //Select SMCLK as input clk but still keep the port in reset
    UCB0CTL1 |= UCSSEL_2 + UCSWRST;

//    UCB0BR0  = 10;      //1MHz / 10 = 100KHz
    UCB0BR0  = 160;      //16MHz / 160 = 100KHz

    UCB0BR1  = 0;         //upper divisor

    //take the port out of reset
    UCB0CTL1 &= ~UCSWRST;

}

/**
 * @brief [Obtener Temperatura/Humedad]
 * @details [Funcion para obtener la temperatura y la humedad del sensor]
 */
void HT_get(void)
{

    //Turn on sensor
    UCB0I2CSA = 0x77;               //  Slave Adress >> 1, the read/write bit is not included
    UCB0CTL1 |= UCTR + UCTXSTT;     //  I2C TX, start condition send
    while(!(IFG2 & UCB0TXIFG));     //  wait for Master to stop sending
    UCB0TXBUF = 0x1E;               //  Power on reset Command
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?
    while(IFG2 & UCB0TXIFG);        //  Transmit interrupts pending
    while(!(IFG2 & UCB0TXIFG));     //
    UCB0CTL1 |= UCTXSTP;            //  Stop condition send
    while(UCB0CTL1 & UCTXSTP);      //  Stop condition sent?

    for (i = 150; i>=0; i--)
        {
            __no_operation();
        }
    _no_operation();

    //Read calibration values
    //Calibration data 1
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xA2;                   //Read Coefficient 1
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 2; i++)
    {
        if(i == 1)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    C1 = (Read_Buffer_Array[0] << 8 | Read_Buffer_Array[1]);

    for (i = 150; i>=0; i--)
    {
        _no_operation();
    }
    _no_operation();

    //calibration data 2
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xA4;                   //Read Coefficient 2
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 2; i++)
    {
        if(i == 1)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    C2 = (Read_Buffer_Array[0] << 8 | Read_Buffer_Array[1]);

    for (i = 150; i>=0; i--)
    {
        _no_operation();
    }
    _no_operation();

    //calibration data 3
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xA6;                   //Read Coefficient 3
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 2; i++)
    {
        if(i == 1)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    C3 = (Read_Buffer_Array[0] << 8 | Read_Buffer_Array[1]);

    for (i = 150; i>=0; i--)
    {
        _no_operation();
    }
    _no_operation();

    //calibration data 4
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xA8;                   //Read Coefficient 8
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 2; i++)
    {
        if(i == 1)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    C4 = (Read_Buffer_Array[0] << 8 | Read_Buffer_Array[1]);

    for (i = 150; i>=0; i--)
    {
        _no_operation();
    }
    _no_operation();


    //calibration data 5
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xAA;                   //Read Coefficient 5
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 2; i++)
    {
        if(i == 1)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    C5 = (Read_Buffer_Array[0] << 8 | Read_Buffer_Array[1]);

    for (i = 150; i>=0; i--)
    {
        _no_operation();
    }
    _no_operation();



    //calibration data 6
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xAC;                   //Read Coefficient 1
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 2; i++)
    {
        if(i == 1)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    C6 = (Read_Buffer_Array[0] << 8 | Read_Buffer_Array[1]);

    for (i = 150; i>=0; i--)
    {
        _no_operation();
    }
    _no_operation();

    //CRC
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0xAE;                   //Read CRC
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 1; i++)
    {
        if(i == 0)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);

    ms_crc = Read_Buffer_Array[0];




    for (i = 150; i>=0; i--)
    {
        __no_operation();
    }

    __no_operation();

    //COnversion sequence-Pressure
    //Initiate pressure conversion
    UCB0I2CSA = 0x77;               //  Slave Adress >> 1, the read/write bit is not included
    UCB0CTL1 |= UCTR + UCTXSTT;     //  I2C TX, start condition send
    while(!(IFG2 & UCB0TXIFG));     //  wait for Master to stop sending
    UCB0TXBUF = 0x48;               //  Initiate pressure conversion command
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?
    while(IFG2 & UCB0TXIFG);        //  Transmit interrupts pending
    while(!(IFG2 & UCB0TXIFG));     //
    UCB0CTL1 |= UCTXSTP;            //  Stop condition send
    while(UCB0CTL1 & UCTXSTP);      //  Stop condition sent?

    for (i = 150*20; i>=0; i--)
        {
            __no_operation();
        }
    _no_operation();

    //Read converted values values
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0x00;                   //???????????????
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 3; i++)
    {
        if(i == 2)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);


    dpv =  ((uint32_t)Read_Buffer_Array[0]<<16 | (uint32_t)Read_Buffer_Array[1]<<8 | (uint32_t)Read_Buffer_Array[2]);

    for (i = 150*2; i>=0; i--)
    {
        __no_operation();
    }
    _no_operation();

    //COnversion sequence-Temperature
    //Initiate temperature conversion
    UCB0I2CSA = 0x77;               //  Slave Adress >> 1, the read/write bit is not included
    UCB0CTL1 |= UCTR + UCTXSTT;     //  I2C TX, start condition send
    while(!(IFG2 & UCB0TXIFG));     //  wait for Master to stop sending
    UCB0TXBUF = 0x58;               //  Initiate temperature conversion command
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?
    while(IFG2 & UCB0TXIFG);        //  Transmit interrupts pending
    while(!(IFG2 & UCB0TXIFG));     //
    UCB0CTL1 |= UCTXSTP;            //  Stop condition send
    while(UCB0CTL1 & UCTXSTP);      //  Stop condition sent?

    for (i = 150*20; i>=0; i--)
        {
            __no_operation();
        }
    _no_operation();

    //Read converted values values
    UCB0CTL1 |= UCTR + UCTXSTT;         //  I2C TX, start condition send
    UCB0TXBUF = 0x00;                   //Adc Read command
    while (UCB0CTL1 & UCTXSTT);         //  Start condition sent?

    UCB0CTL1 &= ~UCTR;              //Master, read mode
    UCB0CTL1 |= UCTXSTT;            //  Start condition send;
    while(UCB0CTL1 & UCTXSTT);      //  Start condition sent?


    for(i = 0; i < 3; i++)
    {
        if(i == 2)
        {
            UCB0CTL1 |= UCTXSTP;    //  Stop condition send
        }
        while(!(IFG2 & UCB0RXIFG)); //Pending data to receive?
        Read_Buffer_Array[i] = UCB0RXBUF;   //  Store data
    }

    while(UCB0CTL1 & UCTXSTP);


    dtv =  (float)((uint32_t)Read_Buffer_Array[0]<<16 | (uint32_t)Read_Buffer_Array[1]<<8 | (uint32_t)Read_Buffer_Array[2]);

    for (i = 150*2; i>=0; i--)
    {
        __no_operation();
    }
    _no_operation();


//    uint32_t dT = dt_calc();
//
//    uint32_t a1 = 0;
//    uint32_t a2 = 10000;
//    a3 = 2*a2;
    dT = dtv - ((float)C5*256);
    Temp = 2000 + (dT*(float)C6)/8388608;



    dTd = Temp/1000;
    tempTemp = (uint16_t)Temp%1000;
//    tempTemp = Temp%1000;

    dTu = tempTemp/100;
    tempTemp = tempTemp%100;

    dTdd = tempTemp/10;
    dTddd = tempTemp%10;

//    Presion calculo
    OFF = ((float)C2*65536)+((float)C4*dT)/128;
    SENS = ((float)C1*32768)+((float)C3*dT)/256;
    P = (((dpv*SENS)/2097152)-OFF)/32768;

    dPm = P/100000;
    tempP = (uint32_t)P%100000;

    dPc   = tempP/10000;
    tempP = tempP%10000;

    dPd   = tempP/1000;
    tempP = tempP%1000;

    dPu   = tempP/100;
    tempP = tempP%100;

    dPdd  = tempP/10;
    dPddd = tempP%10;

}


/**
 * @brief [Funcion principal]
 * @details [Funcion principal que corre ciclicamente]
 * @return [nada]
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	ConfigUSCIB0_I2C();
	//return 0;

	while(1)
	{
	    HT_get();
	}
}
