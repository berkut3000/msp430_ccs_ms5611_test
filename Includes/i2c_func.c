/*
 * i2c_func.c
 *
 *  Created on: 10/04/2018
 *      Author: hydro
 */
#include "i2c_func.h"

unsigned char RXData;

void i2c_conf()
{
  P1SEL |= BIT6 + BIT7;                     // Assign I2C pins to USCI_B0
  P1SEL2|= BIT6 + BIT7;                     // Assign I2C pins to USCI_B0
  UCB0CTL1 |= UCSWRST;                      // Enable SW reset
  UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC;     // I2C Master, synchronous mode
  UCB0CTL1 = UCSSEL_2 + UCSWRST;            // Use SMCLK, keep SW reset
  UCB0BR0 = 12;                             // fSCL = SMCLK/12 = ~100kHz
  UCB0BR1 = 0;
  UCB0I2CSA = 0x048;                        // Slave Address is 048h
  UCB0CTL1 &= ~UCSWRST;                     // Clear SW reset, resume operation
  IE2 |= UCB0RXIE;                          // Enable RX interrupt
}

void i2c_start_send()   // I2C start condition
{
    UCB0CTL1 |= UCTXSTT;
}

void i2c_start_sent()   // Start condition sent?
{
    while (UCB0CTL1 & UCTXSTT);
}

void i2c_stop_send()   // I2C stop condition
{
    UCB0CTL1 |= UCTXSTP;
}

void i2c_stop_sent()    // Ensure stop condition got sent
{
    while (UCB0CTL1 & UCTXSTP);
}

// USCI_B0 Data ISR
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCIAB0TX_VECTOR))) USCIAB0TX_ISR (void)
#else
#error Compiler not supported!
#endif
{
  RXData = UCB0RXBUF;                       // Get RX data

//  if(RXData ==0 || RXData == 255)
//  {
//      P1OUT |= BIT4;
////      P2OUT ^= BIT4;
//  }
//  else{
//      P1OUT &= ~BIT4;
//  }

  __bic_SR_register_on_exit(CPUOFF);        // Exit LPM0
}


